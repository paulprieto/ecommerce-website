// select add to cart field

let addTocartField = document.querySelectorAll('.add-to-cart-field');

if (addTocartField) {
	// console.log(addTocartField)
	function isMinLimit(input) {
		if (input.value <= 1)
			return true;
		else
			return false;
	}

	function isMaxLimit(input){
		if(input.value >=99)
			return true;
		else
			return false;
	}


	let deductQuantityBtns = document.querySelectorAll('.deduct-quantity');
	let addQuantityBtns = document.querySelectorAll('.add-quantity');

	deductQuantityBtns.forEach( function(deductQuantityBtn){
		let deductQuantityBtnDataID = deductQuantityBtn.dataset.id;
		let inputTarget = document.querySelector('input[data-id="'+ deductQuantityBtnDataID +'"]');

		deductQuantityBtn.addEventListener('click', function() {
			if (!isMinLimit(inputTarget))
				inputTarget.value -= 1;
			else
				inputTarget.value = 1;
		});
	});

	addQuantityBtns.forEach( function(addQuantityBtn){
		let addQuantityBtnDataID = addQuantityBtn.dataset.id;
		let inputTarget = document.querySelector('input[data-id="'+ addQuantityBtnDataID +'"]');

		addQuantityBtn.addEventListener('click', ()=> {
			if (!isMaxLimit(inputTarget)) 
				inputTarget.value = parseInt(inputTarget.value) + 1;
			else
				inputTarget.value =99;
		});
	});

	let inputQuantities = document.querySelectorAll('.input-quantity');
	// console.log(inputQuantities);
	inputQuantities.forEach((inputQuantity)=> {
		inputQuantity.addEventListener('input',function(event){
			console.log(event);
			if(isMinLimit(inputQuantity)){
				inputQuantity.value =0;
			}
			if (isMaxLimit(inputQuantity)) {
				inputQuantity.value = 99;
			}
		});

		// Select all input on click
		inputQuantity.addEventListener('click', ()=> {
			inputQuantity.select();
		})
	});

}