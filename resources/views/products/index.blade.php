@extends('layouts.app')

@section('content')
	
	<div class="container">
		<div class="row">
			<div class="col-12 ">
				<div class="jumbotron text-center header">
					<h1>
						<i class="fab fa-empire"></i> MJSA Military Supply Co. <i class="fab fa-empire"></i>
					</h1>
				</div>
			</div>
		</div>
		@if (Session::has('destroy_success'))
			<div class="row">
				<div class="col-12">
					<div class="alert alert-success">
						{{ Session::get('destroy_success') }}
					</div>
				</div>
			</div>
		@endif

		<div class="row">
			<div class="col-12 col-md-10 mx-auto">
				<div class="row">
					@foreach ($products as $product)
						<div class="col-12 col-md-4">
							<div class="card shadow p-3 mb-5 bg-white rounded">
								<img 
								src="{{ url('/public/' . $product->image )  }}" 
								class="card-img-top"
								>
								<h5 class="card-title">
									{{ $product->name }}
								</h5>
								<p class="card-subtitle">
									<strong>
										&#8369; {{ $product->price }}
									</strong>
								</p>
								<p> {{ $product->category->name }} </p>



								<div class="card-footer">

									@cannot('isAdmin')
									<form action="{{ route('carts.store') }}" method="POST" class="add-to-cart-field">
										@csrf
										<input type="hidden" name="id" value="{{ $product->id }} ">

										<div class="form-group">
											<label for="quantity"><small>Quantity: </small></label>
											<div class="input-group">
												<input type="text" class="form-control input-quantity" value="1" min="1" name="quantity" data-id="{{ $product->id }}" >
												<div class="input-group-append" id="button-addon4">
													<button class="btn btn-outline-success deduct-quantity" type="button" data-id="{{ $product->id }}">-</button>
													<button class="btn btn-outline-success add-quantity" type="button" data-id="{{ $product->id }}">+</button>
												</div>
											</div>
										</div>
										<button class="btn btn-success btn-block my-1"><i class="fas fa-cart-plus"></i> Add To Cart</button>
										{{-- <a type="submit" class="btn btn-success btn-block my-1">
											Add To Cart
										</a> --}}
									</form>
									@endcannot

									<a 
									href="{{ route('products.show', ['product' => $product->id])}}" 
									class="btn btn-secondary btn-block my-1">View Product
									</a>
									
									@can('isAdmin')
									<a 
									href="{{ route('products.edit', ['product' => $product->id]) }} " 
									class="btn btn-block btn-warning my-1">Edit Product
									</a>


									<form action="{{ route('carts.destroy', ['product' => $product->id]) }}" method="POST">
										@csrf
										@method('DELETE')
										<button class="btn btn-danger btn-block my-1">Delete Product</button>
									</form>
									@endcan

								</div>
							</div>
						</div>
					@endforeach
				</div>


			</div>
		</div>
	</div>

@endsection