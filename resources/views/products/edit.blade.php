
@extends('layouts.app')


@section('content')


	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<h3 class="text-center">
					Add Product
				</h3>
				<hr>
				@if(Session::has('update_failed'))
					<div class="alert alert-warning">
						{{ Session::get('update_failed') }}
					</div>
				@endif

				@if (Session::has('update_success'))
					<div class="alert alert-success">
						{{ Session::get('update_success') }}
					</div>
				@endif
				<form action="{{ route('products.update', ['product' => $product->id]) }}" method="POST" enctype="multipart/form-data" >
					
					@csrf
					@method('PUT')

					<div class="form-group">
						<label for="name">Product Name:</label>
						<input type="text" name="name" id="name" class="form-control" value="{{$product->name}}">

						@if ($errors->has('name'))
							<div class="alert alert-danger">
								<small class="mb-0">Product name is required.</small>
							</div>
						@endif
					</div>

					{{-- Input for price --}}
					<div class="form-group">
						<label for="price">Product Price:</label>
						<input  type="number" name="price" id="price" class="form-control" min="1" value="{{$product->price}}">
						@if ($errors->has('price'))
							<div class="alert alert-danger">
								<small class="mb-0">Product price is required.</small>
							</div>
						@endif
					</div>

					{{-- Input for category --}}
					<div class="form-group">
						<label for="category">Product Category:</label>
						<select class="form-control" id="category" name="category">

							@foreach ($categories as $category)
								<option 
									value="{{ $category->id }}"
									@if ($product->category_id == $category->id)
										selected 
									@endif
								>
									{{ $category->name }}
								</option>
							@endforeach
							
					    </select>
					</div>


					{{-- Input for image --}}
					<div class="form-group">
						<label for="image">Product Image:</label>
						<input type="file" name="image" id="image" class="form-control-file" > 
						@if ($errors->has('image'))
							{{-- {{dd($errors)}} --}}
							<div class="alert alert-danger">
								<small class="mb-0">Product image is required. Check if image is not greater than 3mb.</small>
							</div>
						@endif
					</div>

					{{-- Input for description --}}
					<div class="form-group">
						<label for="description">Product Description:</label>
						<textarea 
							name="description" 
							id="description" 
							class="form-control" 
							min="1" 
							cols="30" 
							rows="10"
						>{{ $product->description}}</textarea>


						@if ($errors->has('description'))
							<div class="alert alert-danger">
								<small class="mb-0">Product description is required.</small>
							</div>
						@endif
					</div>

					<button class="btn btn-warning btn-block">Update</button>

				</form>

			</div>
		</div>
	</div>

@endsection