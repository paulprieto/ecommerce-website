
@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3 class="text-center">
					My Cart Details
				</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-12 col-md-10 mx-auto">
				{{-- if the count of session cart is more than zero and session have a cart session --}}
				@if (Session::has('cart') && count(Session::get('cart')))
					<div class="table-responsive">
						 <table class="table table-striped table-hover text-center">
							<thead>
								<tr>
									<th>Name</th>
									<th>Unit Price</th>
									<th>Quantity</th>
									<th>Subtotal</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								{{-- {{dd(Session::get('cart'))}} --}}
								{{-- {{ dd($products) }}	 --}}
								@foreach ($products as $product)	
									<tr>
										<th scope="row">{{ $product->name }}</th>
										<td>&#8369; <span class="product-price">{{ $product->price }}</span></td>
										<td>
											<form action="{{ route('carts.store', ['id' ,$product->id]) }}" method="POST" class="add-to-cart-field">
												@csrf
												<input type="hidden" name="id" value="{{ $product->id }} ">
												<div class="form-group">
													<div class="input-group">
														<input type="text" class="form-control input-quantity" min="1" name="quantity" data-id="{{ $product->id }}" value="{{ $product->quantity }}" >
														<div class="input-group-append" id="button-addon4">
															<button class="btn btn-outline-success deduct-quantity" type="button" data-id="{{ $product->id }}">-</button>
															<button class="btn btn-outline-success add-quantity" type="button" data-id="{{ $product->id }}">+</button>
														</div>
													</div>
												</div>
												<button class="btn btn-success btn-block">Update Quantity</button>
												{{-- <a type="submit" href="" class="btn btn-success btn-block my-1">
													Update Quantity
												</a> --}}
											</form>
										</td>
										<td>
											&#8369; <span class="product-subtotal">{{ $product->subtotal }}</span>
										</td>
										<td>
											<form method="POST" action="{{ route('carts.destroy', ['cart' => $product->id]) }}">
												@csrf
												@method('DELETE')
												<button class="btn btn-danger btn-block">Remove from cart</button>
											</form>
										</td>
									</tr>
								@endforeach
								
							</tbody>
							<tfoot>
								<tr>
									<td  colspan="3" class="text-right"  >Total</td>
									<td> &#8369; <span class="total_price">{{ $total }}</span></td>
									<td>
										@if(Auth::user())
										<form method="post" action="{{ route('transactions.store') }}" >
											@csrf
											<button class="btn btn-primary btn-block">Checkout Cart</button>
										</form>

										@else
											{{-- <a href="{{ route('login') }}" class="btn btn-primary btn-block">Checkout Cart</a> --}}
											<button class="btn btn-primary btn-block" data-toggle="modal" data-target="#auth-modal" >Checkout Cart</button>
										@endif

										@guest

										@else
										<hr>
										<p class="mb-0"><small>Pay via:</small></p>
										<div class="py-3" id="paypal-button-container"></div>
										@endguest

									</td>
								</tr>

							</tfoot>		 	
						 </table>
					</div>

				<form action="{{ route('carts.clear') }}" method="POST">
					@csrf
					@method('DELETE')
					<button class="btn btn-danger w-100 ">Empty cart</button>
				</form>

				@else
					@if(Session::has('Checkout_success'))
						<div class="alert alert-success text-center ">
							{{ Session::get('Checkout_success') }}
						</div>
					@else
					
					<div class="text-center alert alert-warning">
						<h5>No items in cart. </h5>
						<small><a href="{{ route('products.index') }}">Go to Products</a></small>
					</div>
					@endif
				@endif
			</div>
		</div>
	</div>


	{{-- MODAL --}}
	<div class="modal fade" id="auth-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Login</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>

			</div>
		</div>
	</div>


	@if(Session::has('cart') && count(Session::get('cart')))
	<script 
		src="https://www.paypal.com/sdk/js?client-id=AXjMwT4BNblmJ1gZFvkoNI_kd8fQD6bMtEd4WqZDfK0Hb3v9PtF8QeqG97b7stuvZPjWighBJiCEyk7E">
	</script>

	<script>
		paypal.Buttons({
	    createOrder: function(data, actions) {
	      // Set up the transaction
	      return actions.order.create({
	        purchase_units: [{
	          amount: {
	            value: '{{ $total }}'
	            //you can also use document.querySelector('#total_price').innerHTML
	          }
	        }]
	      });
	    },
	    onApprove: function(data, actions) {
			return actions.order.capture().then(function(details) {
				// console.log(details.id);


				fetch('http://localhost:8001/transaction/paypal/create', {
					method : 'post',
					body : JSON.stringify({ 
						transactionNumber: details.id
					}),
					headers : {
						'X-CSRF-TOKEN' : document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
						'Accept' : "application/json"
					}
				})
				.then( response => response.json())
				.then( result => {
					window.location = result.url
					// console.log(result.url);
				});


				// alert('Transaction completed by ' + details.payer.name.given_name);
				// // Call your server to save the transaction
				// return fetch('/paypal-transaction-complete', {
				// 	method: 'post',
				// 	headers: {
				// 		'content-type': 'application/json'
				// 	},
				// 	body: JSON.stringify({
				// 		orderID: data.orderID
				// 	})
				// });
			});
	    }
	  }).render('#paypal-button-container');

	   
	</script>
	@endif
@endsection
