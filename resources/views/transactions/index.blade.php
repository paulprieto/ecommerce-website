@extends('layouts.app')


@section('content')



<div class="container-fluid">
	<div class="row">
		<div class="col-12 col-md-8 mx-auto">

			<h2 class="text-center">Transactions</h2>

			<div class="accordion" id="transactionAccordion">

				{{-- {{ dd(Session::all()) }} --}}

				@foreach ($my_transactions as $transaction)
				<div class="card shadow bg-white rounded">
					<div class="card-header" id="headingOne">
						<h2 class="mb-0">
							<button 
								class="btn btn-block text-left" 
								type="button" 
								data-toggle="collapse" 
								data-target="#transaction-{{$transaction->id }}" 
								aria-expanded="false" 
								aria-controls="collapseOne">
								{{$transaction->transaction_number }} / {{ $transaction->created_at->format('F d, Y - h:i') }}  
								@if (Session::has('transaction_updated') && Session::get('transaction_updated') == $transaction->id )
									<span class="badge badge-info"> Status Updated </span>
								@endif

								@if ($transaction->status->name == "Pending")
									<span class="badge badge-warning float-right">{{$transaction->status->name }}</span>

								@else
									<span class="badge badge-success float-right">{{$transaction->status->name }}</span>

								@endif
								
							</button>

						</h2>
					</div>

					<div id="transaction-{{$transaction->id }}" class="collapse" aria-labelledby="headingOne" data-parent="#transactionAccordion">
						<div class="card-body">
							<h5 class="card-title text-center">Summary</h5>
							<div class="table-responsive mb-3">

								<table class="table table-sm table-borderless">
									<tbody>
										<tr>
											<td>Customer Name:</td>
											<td><strong>{{$transaction->user->name }}</strong></td>
										</tr>
										<tr>
											<td>Transaction Number:</td>
											<td><strong>{{ $transaction->transaction_number }}</strong></td>
										</tr>
										<tr>
											<td>Mode of Payment:</td>
											<td>{{ $transaction->payment_mode->name }}</td>
										</tr>
										<tr>
											<td>Status</td>
											<td>{{ $transaction->status->name }}

												@can('isAdmin')
												<form action="{{ route('transactions.update', ['transaction'=> $transaction->id]) }} " method="POST" class="p-3 bg-secondary rounded">
													@csrf
													@method('PUT')

													<label for="edit-transaction-{{$transaction->id}}">Change Status</label>
													<button class="btn btn-primary mb-1 float-right ">Change Status</button>
													<span>
														<select class="custom-select mb-1" id="edit-transaction-{{$transaction->id}}" name="status">
															@foreach($statuses as $status)

																<option value="{{ $status->id}} " 
																	@if ($transaction->status_id == $status->id)
																		selected
																	@endif
																> {{$status->name }} </option>

															@endforeach
														</select>
													</span>
												</form>
												@endcan
											</td>
										</tr>
										<tr>
											<td>Date</td>
											<td>{{$transaction->created_at->format('F d, Y')}}</td>
										</tr>
										<tr>
											<td>Total</td>
											<td> &#8369; {{ number_format($transaction->total, 2) }} </td>
										</tr>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="2">
												<a href=" {{ route('transactions.show', ['transaction'=>$transaction->id]) }} " class="page-link text-center rounded">View Details</a>
											</td>
										</tr>
									</tfoot>
								</table>

							</div>
						</div>
					</div>
				</div>

				@endforeach

		</div>
	</div>
</div>




@endsection