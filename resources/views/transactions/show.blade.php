@extends('layouts.app')

@section('content')
	{{-- {{$transaction->user->name}} --}}



{{-- 	@foreach ($transaction->products as $transaction_product)
		{{ $transaction_product->name }}


	@endforeach --}}

	<div class="container">
		<div class="row">
			<div class="col-12 col-md-12 mx-auto">
				<h3 class="text-center">Transaction Details</h3>
				<hr>
			</div>
			{{-- end of col --}}
		</div>
		{{-- end of row --}}


		<div class="row">
			<div class="col-12 col-md-12 mx-auto">

				<div class="table-responsive">
					<table class="table table-sm table-borderless">

						<tbody>
							<tr>
								<td>Customer Name:</td>
								<td><strong>{{$transaction->user->name}}</strong></td>
							</tr>
							<tr>
								<td>Transaction Number:</td>
								<td><strong>{{$transaction->transaction_number}}</strong></td>
							</tr>
							<tr>
								<td>Mode of Payment:</td>
								<td> {{ $transaction->payment_mode->name }} </td>
							</tr>
							<tr>
								<td>Status</td>
								<td>{{$transaction->status->name}}</td>
							</tr>
							<tr>
								<td>Date</td>
								<td>{{$transaction->created_at->format('F d, Y')}}</td>
							</tr>

						</tbody>





					</table>
					{{-- end of table --}}
					<hr>
					<table class="table table-borderless">
						<thead>
							<th scope="col">Product Name</th>
							<th scope="col">Quantity</th>
							<th scope="col">Unit Price</th>
							<th scope="col" class="text-center">Amount</th>
						</thead>
						<tbody>
							@foreach ($transaction->products as $transaction_product)
							<tr>

								<td>{{$transaction_product->name }}</td>
								<td>{{$transaction_product->pivot->quantity }}</td>
								<td>&#8369; 
									<span>
										{{ number_format($transaction_product->pivot->price) }}
									</span>
								</td>
								<td class="text-center">&#8369; 
									<span>
										{{ number_format($transaction_product->pivot->subtotal) }}
									</span>
								</td>

							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th scope="row" colspan="3" class="text-right">Total</th>
								<td scope="row" colspan="3" class="text-center">&#8369; 
									<span>
										{{ number_format($transaction->total) }}
								</span></td>
							</tr>

							
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>

@endsection