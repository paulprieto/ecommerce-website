<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categories')->insert([
        	'name'=>'Equipments'
        ]);
        DB::table('categories')->insert([
        	'name'=>'Pants'
        ]);
        DB::table('categories')->insert([
        	'name'=>'Jackets'
        ]);
    }
}
