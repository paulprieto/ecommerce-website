<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', function () {
    return redirect(route('products.index'));
});


Route::get('/home', 'ProductController@index');

// Route::get('/cart', function() {
// 	return view('cart');
// })->name('cart');

Route::post('transaction/paypal/create', 'TransactionController@paypal_store');

Route::delete('/carts/clear', 'CartController@clearCart')->name('carts.clear');

Route::resource('products', 'ProductController');

Route::resource('categories', 'CategoryController');

Route::resource('carts', 'CartController');

Route::resource('transactions', 'TransactionController');
