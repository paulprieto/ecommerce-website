<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Str;
// this will get all the category on the database to pass it to create form
use App\Category;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('products.index')->with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Product $product)
    {
        $this->authorize('create', $product);

        $categories = Category::all();
        return view('products.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product)
    {

        $this->authorize('create', $product);

        // FLOW
        // 1. validate each field (validation errors will display in views)
        // 2. save uploaded image (because we need the path. To get the the path which will be saved to the database.)
        // 3. get input values from the form
        // 4. save the details to the database
        // 5. redirect to a single product redirect(show)

        // 1
        $request->validate([
            'name' => 'required|string',
            'price' => 'required|numeric',
            'category' => 'required',
            'image' => 'required|image|max:30000',
            'description' => 'required|string'
        ]);

        // 2.1 get uploaded file

        // 2.2 get uploaded filename

        // 2.3 get uploaded file extension name

        // 2.4 generate new name with random characters

        // 2.5 save the new file to public folder

        // 2.1
        $file = $request->file('image');

        // 2.2
        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        
        // 2.3
        $file_extension = $file->extension();

        // 2.4
        $random_chars = Str::random(10);

        $new_file_name = date('Y-m-d-H-i-s') . "_" . $random_chars . "_" . $file_name . "." . $file_extension;

        // dd($new_file_name);

        // 2.5
        $file_path = $file->storeAs('images', $new_file_name, 'public');


        // 3. get input values from form
        // $data = $request->all(
        //     'name',
        //     'price',
        //     'category',
        //     'description'
        // );
        
        $name = $request->input('name');
        $price = $request->input('price');
        $category_id = $request->input('category');
        $description = $request->input('description');
        $image = $file_path;

        // 4. save details to database
        $product = new Product;
        $product->name = $name;
        $product->price = $price;
        $product->category_id = $category_id;
        $product->description = $description;
        $product->image = $image;
        $product->save();

        // 5. redirect
        return redirect(route( 'products.show', ['product' => $product->id] ));


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $this->authorize('update', $product);

        $categories = Category::all();
        return view('products.edit')->with('product', $product)->with('categories', $categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        // FLOW
        // 1. check if all fields are not empty except image
        // 2. Update database if there are differences between the details from input form and the current details of the product

        $this->authorize('update', $product);
        // 1.
        $request->validate([
            'name' => 'required|string',
            'price' => 'required|numeric',
            'category' => 'required',
            // 'image' => 'required|image|max:30000',
            'description' => 'required|string'
        ]);

        // 2.
        

        if ( 
            !$request->hasFile('image') &&
            $product->name == $request->input('name') &&
            $product->price == $request->input('price') &&
            $product->category_id == $request->input('category') &&
            $product->description == $request->input('description')
        ) {
            $request->session()->flash('update_failed','There are no changes made.');
            
        }
        else {
            // update the entry in the database and return the updated entry

            // check if there is a file uploaded
            if($request->hasFile('image')){
                $request->validate([
                    'image' => 'required|image|max:30000'
                ]);

                $file = $request->file('image');
                $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $file_extension = $file->extension();
                $random_chars = Str::random(10);
                $new_file_name = date('Y-m-d-H-i-s') . "_" . $random_chars . "_" . $file_name . "." . $file_extension;
                $file_path = $file->storeAs('images', $new_file_name, 'public');
                $product->image = $file_path;
            }

            // set the new image path as image value of product
            $product->name = $request->input('name');
            $product->price = $request->input('price');
            $product->category_id = $request->input('category');
            $product->description = $request->input('description');
            // dd($product);
            $product->save();

            $request->session()->flash('update_success', 'Product successfully updated');

        }
        return redirect(route('products.edit', ['product' => $product->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Product $product)
    {
        $this->authorize('delete', $product);
        $product->delete();
        // $request->session()->flash('destroy_success', 'Product deleted');
        return redirect(route('products.index'))->with('destroy_success', 'Product Deleted.');
    }
}
