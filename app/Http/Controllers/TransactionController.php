<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Product;
use App\Status;

use Illuminate\Http\Request;
use Session;
use Auth;
Use Str;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        if (Auth::user()->role_id === 2) {
            $statuses = Status::all();
            $user_id = Auth::user()->id;
            $transactions = Transaction::where('user_id', $user_id)->orderBy('created_at', 'DESC')->get();

            //  $transaction = Transaction::where('payment_modes', $user_id);
             // $transaction = Transaction::where('column from Transation', value to be searched);
            // dd($transactions);
            return view('transactions.index')
                ->with('my_transactions', $transactions)
                ->with('statuses', $statuses);
        }
        
        if (Auth::user()->role_id === 1) {
            $statuses = Status::all();
            $transactions = Transaction::orderBy('created_at', 'DESC')->get();

            //  $transaction = Transaction::where('payment_modes', $user_id);
             // $transaction = Transaction::where('column from Transation', value to be searched);
            // dd($transactions);
            return view('transactions.index')
                ->with('my_transactions', $transactions)
                ->with('statuses', $statuses);
        }

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Transaction $transaction)
    {
        // Flow
        // 1. Create an entry in Transactions table with the following attributes
        //      transaction number
        //      user_id
        //      payment_mode
        //      status
        // 2. add entries to transaction_orders linked to the newly created entry in the transactions table.
        //      quantity
        //      price
        //      subtotal
        //      product_id
        //      transaction_id
        // 3. compute the total of price
        // 4. update the total attribute of the newly created entry in transaction
        // 5. clear cart session.

        // pre-requisite make seeder for the ff

        // statuses table:
        // pending
        // completed

        // payment_modes table:
        // cash on delivery
        // paypal


        // 1.
        // create transaction code
        $this->authorize('create', $transaction);

        $transaction = new Transaction;

        // userid+random+time()
        $transaction_number = Auth::user()->id . "_" . Str::random(10) . "_" . time();
        // dd($transaction_number);
        
        $user_id = Auth::user()->id;
        // dd($user_id);

        $transaction->transaction_number = $transaction_number;
        $transaction->user_id = $user_id;
        $transaction->save();

        // dd(Session::all());
        // dd(Session::get('cart'));

        // 2. add entries to transaction_orders linked to the newly created entry in the transactions table.
        //  2.1 get the keys from the session; this contains the products and quantity of the user wants to buy
        //  2.2 use the array of product id to find it in the database
        //  2.3 for every product listed in cart, add entry to the pivot table(transaction_product).
        //      quantity
        //      price
        //      subtotal
        //      product_id
        //      transaction_id

        $cart_ids = array_keys(Session::get('cart'));

        $products = Product::find($cart_ids);


        // dd(Session::all());
        
        // $transaction->products()->attach(id of referenced table--product)
        
        // this wont work because we dont have belongToMany @ Product Controller
        // $product->transactions()->attach(id of transaction)

        // $transaction->products()->attach(2)


        foreach (Session::get('cart') as $cart_order_id => $quantity) {
            foreach ($products as $product) {
                if ($product->id == $cart_order_id) {
                    $transaction->products()->attach( 
                        $product->id,
                        [
                            'quantity' => $quantity,
                            'price' => $product->price,
                            'subtotal' => $quantity * $product->price
                        ]
                    );
                }   
            }
        }

        $total = 0;
        // 3. compute the total of price
        $transaction_products = $transaction->products;
        foreach ($transaction_products as $product) {
            # code...
            $total += $product->pivot->subtotal;
        }

        // dd($total);

        // 4. update the total attribute of the newly created entry in transaction

        $transaction->total = $total;
        $transaction->save();

        // 5. clear cart session.

        Session::forget("cart");

        return redirect(route('transactions.show', ['transaction' => $transaction->id ]))->with('Checkout_success', 'Your order has been processed.');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        $this->authorize('view', $transaction);
        return view('transactions.show', ['transaction'=>$transaction]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        $this->authorize('update', $transaction);

        $transaction->status_id = $request->input('status');
        $transaction->save();

        // redirect with() will be read as a session
        return redirect(route('transactions.index'))->with('transaction_updated', $transaction->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }


    public function paypal_store(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $transaction = new Transaction;


        $transaction_number = $data['transactionNumber'];
        $user_id = Auth::user()->id;

        $transaction->transaction_number = $transaction_number;
        $transaction->user_id = $user_id;
        $transaction->payment_mode_id = 2;
        $transaction->save();

        $cart_ids = array_keys(Session::get('cart'));

        $products = Product::find($cart_ids);

        foreach (Session::get('cart') as $cart_order_id => $quantity) {
            foreach ($products as $product) {
                if ($product->id == $cart_order_id) {
                    $transaction->products()->attach( 
                        $product->id,
                        [
                            'quantity' => $quantity,
                            'price' => $product->price,
                            'subtotal' => $quantity * $product->price
                        ]
                    );
                }   
            }
        }

        $total = 0;
        $transaction_products = $transaction->products;
        foreach ($transaction_products as $product) {
            $total += $product->pivot->subtotal;
        }

        $transaction->total = $total;
        $transaction->save();

        Session::forget("cart");

        return ["url" => route('transactions.show', ['transaction' => $transaction->id ]) ];
    }

}
