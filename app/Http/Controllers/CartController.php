<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Transaction;
use Session;


class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Transaction $transaction )
    {
        //Admin should not be able to access this

        // flow
        // if there is session cart:
        // get the ids of each product from our session and put them in an array
        // for each product, add quantity and subtotal as an attribute
        // compute total
        // if the is no session cart, send a message saying "No items in cart"


        // products
            // name
            // price
            // quantity
            // subtotal
        // total


        
        $products = [];
        $total = 0;

        if (Session::has('cart')) {
            # code...
            // get all keys of session
            $product_ids = array_keys(Session::get('cart'));

            // we use the ids to query to the database and select only needed products
            $products = Product::find($product_ids);
            // dd($products);

            foreach ($products as $product) {

                // add the product quantity as an attribute
                $product['quantity'] = Session::get("cart.$product->id");
                $product['subtotal'] = $product['price'] * $product['quantity'];

                $total += $product['subtotal'];

            }

            return view('carts.index')->with('products', $products)->with('total', $total);

        }

        return view('carts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Transaction $transaction)
    {
        
        $request->validate([
            'id' => 'required',
            'quantity' => 'required|numeric|min:1|max:99'
        ]);

        // we want our cart session to look like this format
        // $cart = [
        //     id=>quantity
        //     id=>quantity
        //     id=>quantity
        // ]

        // get the request input
        //
        $product_quantity = $request->input('quantity');
        $id = $request->input('id');

        // set entry to be pass to session
        $add_to_cart = [
            $id => $product_quantity
        ];

        // store to session
        // $request->session()->put("cart", [$add_to_cart]);
        $request->session()->put("cart.$id", $product_quantity);
        return redirect(route('carts.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        // dd($request->input());
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Session::forget("cart.$id");

        return redirect(route('carts.index'));
    }

    public function clearCart()
    {
        Session::forget("cart");
        return redirect(route('carts.index'));
    }
}
