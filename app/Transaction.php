<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    //
    use SoftDeletes;
    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function payment_mode(){
    	return $this->belongsTo('App\Payment_mode');
    }

    public function status(){
    	return $this->belongsTo('App\Status');
    }

    // many to many relation ship
    public function products(){
        // include additional tables
        // second parameter to let the program know which table to use
        return $this->belongsToMany('App\Product', 'transaction_product')
            ->withPivot('subtotal', 'price', 'quantity')
            ->withTimestamps();
    }

}
