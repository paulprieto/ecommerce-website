U.S. Grade Military Decontamination Personal Protective Equipment Kit
5000
equipment.jpg
To tackle a messy task, keep this U.S. Military Decontamination Personal Protective Equipment Kit close at hand. Made of DuPont Tychem 6000 protective material used to shield responders in HazMat emergency situations, it will suit your needs for spray painting, fertilizer applications, oil changes and more. Easily slip into the oversized coverall front zipper with 6 internal slots for holding extra tools. The attached Respirator Fit Hood has a tight-fitting elastic opening to secure a mask in place. Slip on the latex gloves and booties and seal at the seams with a roll of ChemTape for additional protection against leaks. The Carry Case keeps everything together and easy to bring along.


Shop Tuff Clog Buster for Sewers, Pipers, and Drains
1000
equipment2.jpg
Team up the Shop Tuff Clog Buster with your pressure washer and unclog your sewer, pipe or drain the easy way. It's 100' long and flexible and tough enough to work its way through pipes to remove clogs. Works well at up to 4800 PSI and fits most gas trigger guns with the M22 coupling.



Danish Military Surplus MOLLE Equipment Pouch, New
2000
equipment3.jpg
Originally designed to haul mags for the M96 Light Machine Gun. Just the right size for tools, camping gear, ammo, anything you want to keep close. MOLLE attachment straps make it easy to stick this Pouch on your backpack, tactical vest, or belt. Top flap closure has a locking mechanism to keep your stuff from falling out. Large main compartment with hidden smaller pocket inside.


U.S. Military Carry Bag, Woodland Camo, 2 Pack
3500
equipment4.jpg
When you're deciding how much gear to load for a day trip or extended hike you'll be glad you have 2 of these U.S. Military Issue Carry Bags. Constructed of 100% nylon polyester canvas with hook-and-loop closure on top and locking web belt to secure your items. Dual metal ALICE clips also allow you to attach one — or both — carry bags to a larger backpack system.


U.S. Military Surplus Dry Bag
2000
equipment
Ensure vital gear stays high and dry in the most adverse conditions with this U.S. Military Dry Bag. Stow away extra clothing or sensitive electronics. Great for keeping muddy clothing and gear secure during transport, too.


Swiss Military Surplus Laundry Bag
Practically iron-clad, this Swiss Laundry Bag is what you need to haul dirty duds to the most remote laundromat location. Rubberized construction helps keep moisture out, and wipes clean easily. Made to strict Swiss military specs.


NATO Military Surplus Cold Weather Gloves
equipment7
Keep your fingers warm and ready for action with these NATO Military Cold Weather Gloves. Leather on palms and fingers resists abrasion while maintaining grip when the temperature drops. And the soft polyester fleece lining is a real treat for your hands. You get 3 pairs, making it easy to keep spares in your glove box or backpack.


U.S. Military Surplus Gen 3 Level 6 Fleece Pants
Designed and manufactured under U.S. military contract, but never adopted by the U.S. military. Intended to be the matching insulating lower layer to the Level 6 Fleece Jacket (see WX2-166474 for reference). Cuddly soft on the inside. Tough and water-resistant on the outside. Ready for snowmobiling, ice fishing, or clearing out your driveway after the blizzard.

Italian Military Surplus Forestry Patrol Pants
These heavy-duty Italian Military Patrol Pants are a great choice for everyday use in the Great Outdoors. As seen protecting Italian forestry rangers from punishing trails and inhospitable terrain. Packed with pockets for keeping tools and other gear easy to reach. Gently used, with plenty of life left to handle the adventure ahead.

U.S. Military Surplus Propper BDU Tactical Pants
The trusted uniform BDU Pants worn by millions of service members deployed all over the world. Super-strong reinforced stitching. 8 pockets worth of storage options. Not to mention decked out in cool, collectible tiger-stripe ABU camo.



U.S. Military Surplus Polartec Fleece Jacket
jacket
Made of hardy Polartec polyester synthetic fleece that's known to defy cold. Wear it alone on those moderate days, or arm it with another layer to defend against freezing temps. In brand new condition—and without the hefty pricetag that's attached to a brand-name Polartec fleece. Get it here. Get it now. Before the next unexpected round of snow pummels you in the face.


U.S. Military Surplus M65 Jacket Liner
jacket2
Take the chill off…by putting on some warm comfort. Fashioned in classic M65 style, this Military Surplus Jacket Liner is dependable. And comfortable. Wear it as a separate jacket or as a formidable liner with another coat.


Irish Police Surplus Commando Sweater
jacket3
Time-tested Commando design, first used by irregular units during WWII for keeping warm in fluctuating alpine weather conditions. The "wooly pully" has since become a military and police mainstay around the world. This one is Irish police issue, ready to beat the chills during your next patrol.




Username: 0ukq7o6uty

Database name: 0ukq7o6uty

Password: K6b7PYyp9u

Server: remotemysql.com

Port: 3306